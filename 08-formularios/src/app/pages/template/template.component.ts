import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { createLoweredSymbol } from '@angular/compiler';
import { PaisService } from '../../services/pais.service';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

  usuario = {
    nombre: 'Lucas',
    apellido: 'Galetti',
    correo: 'lucas.galetti91@gmail.com',
    pais: 'BOL',
    genero: 'M'
  }

  paises: any[] = [];

  constructor( private paisService: PaisService ) { 

  }

  ngOnInit(): void {
    this.paisService.getPais()
        .subscribe( paises => {
          this.paises = paises;
          
          this.paises.unshift({
            nombre: '[ Seleccione Pais ]',
            codigo: 'CRY'
          });

          // console.log( this.paises );
        });

  }

  guardar( forma: NgForm ){
    console.log(forma.value);
    if( forma.invalid ){
      
      Object.values( forma.controls ).forEach( control => {
        //Aca se podria validar por cada uno de los inputs. 
        control.markAsTouched(); 
      })

      return;
    }

  }

}
