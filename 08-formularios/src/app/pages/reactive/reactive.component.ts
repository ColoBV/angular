import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ValidadoresService } from '../../services/validadores.service';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css']
})
export class ReactiveComponent implements OnInit {

  forma: FormGroup;

  constructor(private fb: FormBuilder ,
              private validadores: ValidadoresService ) { 
    this.crearFormulario();
    this.cargarDataFormulario();
    this.crearListeners();

  }

  ngOnInit(): void {
  }

  get pasatiempos(){
    return this.forma.get('pasatiempos') as FormArray;
  }

  get nombreNoValido(){
    return this.forma.get('nombre').invalid && this.forma.get('nombre').touched
  }

  get apellidoNoValido(){
    return this.forma.get('apellido').invalid && this.forma.get('apellido').touched
  }

  get emailNoValido(){
    return this.forma.get('email').invalid && this.forma.get('email').touched
  }

  get usuarioNoValido(){
    return this.forma.get('usuario').invalid && this.forma.get('usuario').touched
  }

  get distritoNoValido(){
    return this.forma.get('direccion.distrito').invalid && this.forma.get('direccion.distrito').touched
  }

  get ciudadNoValido(){
    return this.forma.get('direccion.ciudad').invalid && this.forma.get('direccion.ciudad').touched
  }

  get pass1NoValido(){
    return this.forma.get('pass1').invalid && this.forma.get('pass1').touched
  }

  get pass2NoValido(){
    const pass1 = this.forma.get('pass1').value;
    const pass2 = this.forma.get('pass2').value;

    return ( pass1 === pass2 ) ? false : true;

  }

  crearFormulario(){
    
    this.forma = this.fb.group( { 
      //Esto hace referencia a un FormControl
      //[Valor por Defecto, Validadores Sincronos, Validadores Asyncronos]
      nombre: ['', [Validators.required,Validators.minLength(5)] ],
      apellido: ['',[ Validators.required,this.validadores.noHerrera]],
      email: ['',[Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'),Validators.required]],
      usuario:['', , this.validadores.existeUsuario ], 
      pass1: ['', Validators.required],
      pass2: ['', Validators.required],
      direccion: this.fb.group({
        distrito: ['', Validators.required],
        ciudad: ['', Validators.required],
      }, {
        //validators: this.validadores.passwordsIguales('pass1','pass2')
      }),
      pasatiempos: this.fb.array([])
      
    });  

  }

  crearListeners(){
    this.forma.valueChanges.subscribe( valor => {
      console.log(valor);
    });

    this.forma.statusChanges.subscribe( status => console.log(status));

  }

  cargarDataFormulario(){
    // this.forma.setValue({ La diferencia de uno con el otro, es que el reset permite evitar ciertos campos
    this.forma.reset({
      nombre: 'Lucas',
      apellido: 'Gallina',
      email: 'lucas.gallina@gmail.com',
      direccion: {
        distrito: 'Carlitos',
        ciudad: 'Pedro'
      }
    });
  }

  agregarPasaTiempo(){
    //Esta esl a magia que yo quiero en parte
    this.pasatiempos.push( this.fb.control('',Validators.required))
  }

  borrarPasaTiempo( i:number ){
    this.pasatiempos.removeAt(i);
  }

  guardar(){
    console.log(this.forma);

    if( this.forma.invalid ){
      return Object.values( this.forma.controls ).forEach( control => {
        
        if( control instanceof FormGroup ){
          Object.values( control.controls ).forEach( control => control.markAsTouched())
        }else{
          control.markAsTouched();
        }
        //Aca se podria validar por cada uno de los inputs. 
        control.markAsTouched(); 
      });
    }
  }
}


