import { HeroesService } from './../heroes/heroes.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html'
})
export class HeroeComponent implements OnInit {

  heroe:any = {};
  
  constructor( private activatedRoute : ActivatedRoute,
               private _heroeService : HeroesService ) { 
    this.activatedRoute.params.subscribe( params => {
      // console.log( params['id'] );
      this.heroe = this._heroeService.getHeroe( params['id']);
    })
    
  }

  ngOnInit(): void {
  }

}
