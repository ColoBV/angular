import { Component, OnInit } from '@angular/core';
import { HeroesService, Heroe } from './heroes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html'
})
export class HeroesComponent implements OnInit {

  heroes: Heroe[] = [];
  constructor(private _heroeServIce : HeroesService,
              private router:Router ) {

  }

  ngOnInit(): void {
    this.heroes = this._heroeServIce.getHeroes();
  }

  verHeroe(index:number){
     this.router.navigate(['/heroe',index]);
  }

}
