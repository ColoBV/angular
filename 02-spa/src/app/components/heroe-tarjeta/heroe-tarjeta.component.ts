import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { Heroe } from '../heroes/heroes.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroe-tarjeta',
  templateUrl: './heroe-tarjeta.component.html'
})
export class HeroeTarjetaComponent implements OnInit {

  @Input() heroe:any = {};
  @Input() index:number;
  
  @Output() hereoeSeleccionado:EventEmitter<number>;

  constructor( private router:Router) {
    this.hereoeSeleccionado = new EventEmitter();
  }

  ngOnInit(): void {
  }

  verHeroe(){
    this.router.navigate(['/heroe',this.index]);
    // this.hereoeSeleccionado.emit(this.index);
  }

}
