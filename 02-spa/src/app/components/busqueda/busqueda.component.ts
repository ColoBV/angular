import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Heroe, HeroesService } from '../heroes/heroes.service';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html'
})
export class BusquedaComponent implements OnInit {

  heroes:Heroe[] = [];
  termino: string;

  constructor( private activatedRouter: ActivatedRoute,
               private heroeService: HeroesService ) { 
               }

  ngOnInit(): void {
    this.activatedRouter.params.subscribe(params => {
        this.termino = params['termino'];
        this.heroes = this.heroeService.buscarHeroes(this.termino);
    })
  }

}
