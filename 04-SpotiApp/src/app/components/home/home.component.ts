import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent implements OnInit {

  loading:boolean;
  nuevasCanciones: any[] = [];
  
  error: boolean = false;
  mensajeError :string;

  constructor( private spotifyService: SpotifyService ) {
    
    this.spotifyService.getNewReleases()
        .subscribe( (data:any) => {
          console.log( data );
          this.nuevasCanciones = data;
        },( errorSevicio ) => {
          this.loading = false;
          this.error = true;
          this.mensajeError = errorSevicio.error.error.message;
        });


  }

  ngOnInit(): void {
  }

}
