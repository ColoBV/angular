import { HeroeModel } from './../../models/heroe.model';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HeroesService } from '../../services/heroes.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent implements OnInit {

  heroe = new HeroeModel();

  constructor( private heroesService: HeroesService, 
               private route: ActivatedRoute ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    
    if( id !== 'nuevo' ){
      this.heroesService.getHeroe( id )
          .subscribe( (resp: HeroeModel) => {
            console.log( resp );
            this.heroe = resp;
            this.heroe.id = id;
          });
    }
  }

  guardar( form: NgForm ){

    if( form.invalid ){
      console.log('Formulario no valido');
      return;
    }
    
    if( this.heroe.id ){
      
      this.heroesService.actualizarHeroe( this.heroe )
          .subscribe( resp => {
            console.log(resp);
          });

    }else{
      
      this.heroesService.crearHeroe( this.heroe )
          .subscribe( resp => {
            console.log(resp);
          });
    }

  }


}
