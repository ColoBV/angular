import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeroeModel } from '../models/heroe.model';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {
  
  private url: string = 'https://heroesapp-d1500.firebaseio.com';
  

  constructor( private http: HttpClient ) { }
  

  borrarHeroe( id: string ){
    return this.http.delete(`${ this.url }/heroes/${ id }.json`)
  }

  getHeroe( id: string ){
    return this.http.get(`${ this.url }/heroes/${ id }.json`)
  }

  crearHeroe( heroe: HeroeModel ){
    
    return this.http.post(`${ this.url }/heroes.json`,heroe)
        .pipe( 
          map( (resp: any) => {
            heroe.id = resp.name;
          })
        );
  }

  actualizarHeroe( heroe: HeroeModel ){
    const heroeTemp = {
      ...heroe
    };
    
    delete heroeTemp.id;
  
    //El .json es unicamente obligatorio de Firebase. En otro tipo de servicio backend puede obviarse
    return this.http.put(`${ this.url }/heroes/${ heroe.id }.json`,heroeTemp);
  }

  getHeroes(){
    return this.http.get(`${ this.url }/heroes.json`)
               .pipe(
                //  map( resp => this.crearArreglo(resp)) Esto se puede transformar en lo de abajo
                map( this.crearArreglo )
               );
  }

  private crearArreglo( heroesObj: object ){

    const heroes: HeroeModel[] = [];

    if ( heroesObj === null ) { return []; }    
    
    Object.keys( heroesObj ).forEach( key => {
      const heroe: HeroeModel = heroesObj[key];
      heroe.id = key;
      heroes.push( heroe );

    })

    return heroes;
  }

}
