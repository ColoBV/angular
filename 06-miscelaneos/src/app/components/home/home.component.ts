import { Component, OnInit, OnChanges, DoCheck, AfterContentChecked, AfterContentInit, AfterViewInit, AfterViewChecked, OnDestroy, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <app-ng-style></app-ng-style> 
    <app-clases></app-clases>
    <p [appResaltado]="'orange'">
        Hola Mundo
    </p>
    <app-ng-switch></app-ng-switch> 
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit,OnChanges,DoCheck,AfterContentInit,AfterContentChecked,AfterViewInit,AfterViewChecked,OnDestroy {

  constructor() { 
    console.log("Consstructor")
  }
  ngOnDestroy(): void {
    console.log("Ejecutando metodo onDestroy");
  }
  ngAfterViewInit(): void {
    console.log("Ejecutando metodo ngAfterViewInit");
  }
  ngAfterViewChecked(): void {
    console.log("Ejecutando metodo ngAfterViewChecked");
  }
  ngAfterContentChecked(): void {
    console.log("Ejecutando metodo ngAfterContentChecked");
  }
  ngAfterContentInit(): void {
    console.log("Ejecutando metodo ngAfterContentInit");
  }
  ngDoCheck(): void {
    console.log("Ejecutando metodo ngDoCheck");
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log("Ejecutando metodo ngOnChanges");
  }
  ngOnInit(): void {
    console.log("Ejecutando metodo ngOnInit")
  }

}
